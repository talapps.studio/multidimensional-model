﻿namespace MC.Core.Interfaces.Repositories
{
    public interface IUnitOfWork
    {
        IRepository<TEntity> GetRepository<TEntity>() where TEntity : class;
        dynamic GetRepository(Type type);
    }
}
