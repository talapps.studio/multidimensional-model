﻿using Common.Wrappers;
using System.Linq.Expressions;

namespace MC.Core.Interfaces.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        //Get
        Task<IReadOnlyList<TEntity>> GetAllAsync(Expression<Func<TEntity, bool>> expression = null);

        Task<PagedResponse<IReadOnlyList<TEntity>>> GetPagedResponseAsync(int pageNumber, int pageSize, Expression<Func<TEntity, bool>> expression);
        Task<PagedResponse<IReadOnlyList<TEntity>>> GetPagedResponseAsync(int pageNumber, int pageSize, Expression<Func<TEntity, bool>> expression, string include = "");
      
        Task<TEntity> GetOneByAsync(Expression<Func<TEntity, bool>> filter);
        Task<TEntity> GetOneByAsync(Expression<Func<TEntity, bool>> filter, string include = "");

        Task<TEntity> GetOneByAsyncAsNoTracking(Expression<Func<TEntity, bool>> filter, string include = "");
                
        //Post
        Task<TEntity> AddAsync(TEntity entity);
        Task<TEntity> UpdateAsync(TEntity entity);
               
        
        

        


    }
}
