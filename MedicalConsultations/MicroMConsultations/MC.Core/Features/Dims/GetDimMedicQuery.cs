﻿using Common.Wrappers;
using MC.Core.Dtos;
using MC.Core.Interfaces.Repositories;
using MC.Domain.Views;
using MediatR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MC.Core.Features.Dims
{
    public class GetDimMedicQuery : RequestParameters, IRequest<HttpResponse<PagedResponse<IReadOnlyList<DimMedic>>>>
    {
        public string? Filter { get; set; }
    }

    public class GetDimMedicQueryHandler : IRequestHandler<GetDimMedicQuery, HttpResponse<PagedResponse<IReadOnlyList<DimMedic>>>>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetDimMedicQueryHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task<HttpResponse<PagedResponse<IReadOnlyList<DimMedic>>>> Handle(GetDimMedicQuery request, CancellationToken cancellationToken)
        {
            var result = await _unitOfWork.GetRepository<DimMedic>().GetPagedResponseAsync(request.PageNumber,
                request.PageSize,
                string.IsNullOrEmpty(request.Filter) ? null : GetExpression(request.Filter));

            return new HttpResponse<PagedResponse<IReadOnlyList<DimMedic>>>(new PagedResponse<IReadOnlyList<DimMedic>>()
            {
                CurrentPage = result.CurrentPage,
                PageSize = result.PageSize,
                TotalPage = result.TotalPage,
                TotalRecords = result.TotalRecords,
                Data = result.Data.ToList()
            });

        }

        private static Expression<Func<DimMedic, bool>> GetExpression(string query)
        {
            try
            {
                var parameters = Expression.Parameter(typeof(DimMedic), "x");
                var expression = (Expression)DynamicExpressionParser.ParseLambda(new[] { parameters }, null, query);
                var typedExpression = (Expression<Func<DimMedic, bool>>)expression;
                return typedExpression;
            }
            catch
            {
                throw new ValidationException("filter expression invalid");
            }
        }
    }
}
