﻿
using Common.Wrappers;
using MC.Core.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace MC.Core.Middlewares
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ExceptionMiddleware> _logger;

        public ExceptionMiddleware(RequestDelegate next, ILogger<ExceptionMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex);
                await HandleExceptionAsync(httpContext, ex);
            }
        }

        private async Task HandleExceptionAsync(HttpContext httpContext, Exception exception)
        {
            var response = httpContext.Response;
            response.ContentType = "application/json";
            var error = new HttpResponse<string> { Succeeded = false };

            switch (exception)
            {
                case ValidationException ex:
                    _logger.LogError(ex.Message);
                    error.ErrorCode = 1000;
                    error.ErrorMessage = ex.Message;
                    error.ValidationErrors = ex.Errors;
                    response.StatusCode = StatusCodes.Status400BadRequest;
                    break;
                case InvalidOperationException ex:
                    _logger.LogError(ex.Message);
                    error.ErrorCode = 2000;
                    error.ErrorMessage = ex.Message;
                    response.StatusCode = StatusCodes.Status400BadRequest;
                    break;
                case BadHttpRequestException ex:
                    _logger.LogError(ex.Message);
                    error.ErrorCode = 3000;
                    error.ErrorMessage = ex.Message;
                    response.StatusCode = StatusCodes.Status400BadRequest;
                    break;
                case KeyNotFoundException ex:
                    _logger.LogError(ex.Message);
                    error.ErrorCode = 4000;
                    error.ErrorMessage = ex.Message;
                    response.StatusCode = StatusCodes.Status404NotFound;
                    break;
                default:
                    _logger.LogError("Non controller error");
                    error.ErrorCode = 5000;
                    error.ErrorMessage = $"Non controller error: {exception.Message} {exception.InnerException}";
                    response.StatusCode = StatusCodes.Status500InternalServerError;
                    break;
            }
            string result = JsonConvert.SerializeObject(error);
            await response.WriteAsync(result);
        }
    }
}
