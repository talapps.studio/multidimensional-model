﻿using FluentValidation;
using MC.Core.Behaviours;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace MC.Core
{
    public static class Extension
    {
        public static IServiceCollection AddCore(this IServiceCollection services)
        {
            using var serviceProvider = services.BuildServiceProvider();
            var configuration = serviceProvider.GetService<IConfiguration>();

            services.AddMediatR(Assembly.Load("MC.Core"));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehaviour<,>));
            services.AddValidatorsFromAssembly(Assembly.Load("MC.Core"));
            return services;
        }
    }
}
