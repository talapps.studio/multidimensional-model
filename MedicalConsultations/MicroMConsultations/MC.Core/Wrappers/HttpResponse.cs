﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Wrappers
{
    public class HttpResponse<T>
    {
        public HttpResponse()
        {

        }

        public HttpResponse(T data, string message = null)
        {
            Succeeded = true;
            ResultMessage = message;
            Result = data;
        }


        public bool Succeeded { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public List<string> ValidationErrors { get; set; }
        public T Result { get; set; }
        public string ResultMessage { get; set; }

    }
}
