﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Wrappers
{
    public class HttpApiResponse
    {
        public bool Succeeded { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public List<string> ValidationErrors { get; set; }
        public object Data { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}
