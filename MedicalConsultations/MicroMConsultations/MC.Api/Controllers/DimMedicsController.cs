using Common.Wrappers;
using MC.Core.Features.Dims;
using MC.Domain.Views;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient.Server;

namespace MC.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DimMedicsController : ControllerBase
    {
        private readonly IMediator _mediator;
        public DimMedicsController(IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException();
        }

        [HttpGet]
        public async Task<HttpResponse<PagedResponse<IReadOnlyList<DimMedic>>>> Get([FromQuery] GetDimMedicQuery query)
        {
            return await _mediator.Send(query);
        }
    }
}