﻿
using Microsoft.EntityFrameworkCore;
using MC.Domain.Models;
using MC.Domain.Views;

namespace MC.Persistence.Data
{
    public class ApplicationDbContext : DbContext
    {
       
        public ApplicationDbContext()
        {
        }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        //Tablas
        public DbSet<MedicCenter> MedicCenters { get; set; }
        public DbSet<MedicCenterType> MedicCenterTypes { get; set; }
        public DbSet<Specialty> Specialties { get; set; }
        public DbSet<Medic> Medics { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Appoitment> Appoitments { get; set; }

        //Vistas

        public DbSet<DimMedic> DimMedics { get; set; }

        //public DbSet<DimHospital> Hospitals { get; set; }
        //public DbSet<DimLocations> Locations { get; set; }
        //public DbSet<DimSpecialty> Specialties { get; set; }
        //public DbSet<DimTime> Times { get; set; }
        //public DbSet<MedicalAppointmentsFact> MedicalAppointmentsFacts { get; set; }


        protected override void OnModelCreating(ModelBuilder model)
        {
            base.OnModelCreating(model);
            model.Entity<MedicCenter>(entity =>
            {
                entity.ToTable("MedicCenter");
                entity.HasKey(x => x.Id);
            });
            model.Entity<MedicCenterType>(entity =>
            {
                entity.ToTable("MedicCenterType");
                entity.HasKey(x => x.Id);
            });
            model.Entity<Specialty>(entity =>
            {
                entity.ToTable("Specialty");
                entity.HasKey(x => x.Id);
            });
            model.Entity<Medic>(entity =>
            {
                entity.ToTable("Medic");
                entity.HasKey(x => x.Id);
            });
            model.Entity<Location>(entity =>
            {
                entity.ToTable("Location");
                entity.HasKey(x => x.Id);
            });
            model.Entity<Department>(entity =>
            {
                entity.ToTable("Department");
                entity.HasKey(x => x.Id);
            });
            model.Entity<City>(entity =>
            {
                entity.ToTable("City");
                entity.HasKey(x => x.Id);
            });
            model.Entity<Appoitment>(entity =>
            {
                entity.ToTable("Appoitment");
                entity.HasKey(x => x.Id);
            });


            //model.Entity<DimHospital>(entity =>
            //{
            //    entity.ToTable("Dim_Hospital");
            //    entity.HasKey(x => x.HospitalId);
            //});
            //model.Entity<DimLocations>(entity =>
            //{
            //    entity.ToTable("Dim_Locations");
            //    entity.HasKey(x => x.LocationId);
            //});
            model.Entity<DimMedic>(entity =>
            {
                entity.ToView("DimMedic");
                entity.HasKey(x => x.MedicId);
            });
            //model.Entity<DimSpecialty>(entity =>
            //{
            //    entity.ToTable("Dim_Specialty");
            //    entity.HasKey(x => x.SpecialtyId);
            //});
            //model.Entity<DimTime>(entity =>
            //{
            //    entity.ToTable("Dim_Time");
            //    entity.HasKey(x => x.TimeId);
            //});
            //model.Entity<MedicalAppointmentsFact>(entity =>
            //{
            //    entity.ToTable("MedicalAppointments_Fact");
            //    entity.HasKey(x => x.Id);
            //});

        }



    }
}
