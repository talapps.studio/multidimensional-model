﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using MC.Core.Interfaces.Repositories;
using MC.Persistence.Data;

namespace MC.Persistence.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        private readonly Dictionary<Type, object> _repositories;

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
            _repositories = new Dictionary<Type, object>();
        }

        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : class
        {
            if (_repositories.ContainsKey(typeof(TEntity))) return _repositories[typeof(TEntity)] as IRepository<TEntity>;
            var repository = new Repository<TEntity>(_context);
            _repositories[typeof(TEntity)] = repository;
            return repository;
        }

        public dynamic GetRepository(Type type)
        {
            if (_repositories.ContainsKey(type)) return _repositories[type];
            Type repositoryType = typeof(Repository<>).MakeGenericType(type);
            var repositoryInstance = Activator.CreateInstance(repositoryType, _context);
            _repositories[type] = repositoryInstance;
            return repositoryInstance;
        }
    }
}
