﻿using Common.Wrappers;
using Microsoft.EntityFrameworkCore;
using MC.Core.Interfaces.Repositories;
using MC.Persistence.Data;
using System.Linq.Expressions;

namespace MC.Persistence.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly DbSet<TEntity> _dbSet;
        private readonly ApplicationDbContext _context;
        
        public Repository(ApplicationDbContext context) 
        {
            _dbSet = context.Set<TEntity>();
            _context = context;
            
        }

        public async Task<IReadOnlyList<TEntity>> GetAllAsync(Expression<Func<TEntity, bool>> expression = null)
        {
            IQueryable<TEntity> query = _dbSet.AsNoTracking();
            if (expression != null) query = query.Where(expression);
            return await query.ToListAsync();
        }

        public async Task<PagedResponse<IReadOnlyList<TEntity>>> GetPagedResponseAsync(int pageNumber, int pageSize, Expression<Func<TEntity, bool>> expression = null, string include = "")
        {
            IQueryable<TEntity> query = _dbSet.AsNoTracking();
            if (expression != null) query = query.Where(expression);
            query = include.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
            int totalRecords = query.Count();
            query = query.Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .AsNoTracking();

            return new PagedResponse<IReadOnlyList<TEntity>>()
            {
                TotalRecords = totalRecords,
                TotalPage = totalRecords / pageSize,
                PageSize = pageSize,
                CurrentPage = pageNumber,
                Data = await query.ToListAsync()
            };
        }

        public async Task<PagedResponse<IReadOnlyList<TEntity>>> GetPagedResponseAsync(int pageNumber, int pageSize, Expression<Func<TEntity, bool>> expression)
        {
            IQueryable<TEntity> query = _dbSet.AsNoTracking();
            if (expression != null) query = query.Where(expression);
            int totalRecords = query.Count();
            query = query.Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .AsNoTracking();

            return new PagedResponse<IReadOnlyList<TEntity>>()
            {
                TotalRecords = totalRecords,
                TotalPage = totalRecords / pageSize,
                PageSize = pageSize,
                CurrentPage = pageNumber,
                Data = await query.ToListAsync()
            };
        }
        public async Task<TEntity> GetOneByAsync(Expression<Func<TEntity, bool>> filter)
        {
            IQueryable<TEntity> entity = _dbSet.AsNoTracking();
            return await entity.Where(filter).FirstOrDefaultAsync();
        }
        

        public async Task<TEntity> GetOneByAsync(Expression<Func<TEntity, bool>> filter, string include = "")
        {
            IQueryable<TEntity> entity = _dbSet.AsNoTracking();
            entity = include.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Aggregate(entity, (current, includeProperty) => current.AsSplitQuery().Include(includeProperty));
            var query = entity.Where(filter);
            return await query.FirstOrDefaultAsync(filter);
        }

        

        public async Task<TEntity> GetOneByAsyncAsNoTracking(Expression<Func<TEntity, bool>> filter, string include = "")
        {
            IQueryable<TEntity> entity = _dbSet;
            entity = include.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Aggregate(entity, (current, includeProperty) => current.AsSplitQuery().Include(includeProperty));
            var query = entity.Where(filter).AsNoTracking();//<---
            return await query.FirstOrDefaultAsync(filter);
            
        }
                               
        
        public async Task<TEntity> AddAsync(TEntity entity)
        {
            _dbSet.Add(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task<TEntity> UpdateAsync(TEntity entity)
        {
           
            _dbSet.Update(entity);
            await _context.SaveChangesAsync();
            return entity;
        }
        
        
      

        
    }
}
