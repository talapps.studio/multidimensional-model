﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MC.Persistence.Migrations
{
    public partial class database : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Dim_Hospital",
                columns: table => new
                {
                    HospitalId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Department = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Type = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dim_Hospital", x => x.HospitalId);
                });

            migrationBuilder.CreateTable(
                name: "Dim_Locations",
                columns: table => new
                {
                    LocationId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Country = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    City = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Region = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Longitude = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Latitude = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dim_Locations", x => x.LocationId);
                });

            migrationBuilder.CreateTable(
                name: "Dim_Medic",
                columns: table => new
                {
                    MedicId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    SpecialtyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dim_Medic", x => x.MedicId);
                });

            migrationBuilder.CreateTable(
                name: "Dim_Specialty",
                columns: table => new
                {
                    SpecialtyId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    SubSpecialty = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dim_Specialty", x => x.SpecialtyId);
                });

            migrationBuilder.CreateTable(
                name: "Dim_Time",
                columns: table => new
                {
                    TimeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Year = table.Column<int>(type: "int", nullable: false),
                    Month = table.Column<int>(type: "int", nullable: false),
                    Day = table.Column<int>(type: "int", nullable: false),
                    Hour = table.Column<int>(type: "int", nullable: false),
                    Minutes = table.Column<int>(type: "int", nullable: false),
                    Seconds = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dim_Time", x => x.TimeId);
                });

            migrationBuilder.CreateTable(
                name: "MedicalAppointments_Fact",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SpecialtyId = table.Column<int>(type: "int", nullable: false),
                    LocationId = table.Column<int>(type: "int", nullable: false),
                    HospitalId = table.Column<int>(type: "int", nullable: false),
                    TimeId = table.Column<int>(type: "int", nullable: false),
                    MedicId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    ScheduledAppointments = table.Column<int>(type: "int", nullable: false),
                    ConfirmedAppointments = table.Column<int>(type: "int", nullable: false),
                    CancelledAppointments = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MedicalAppointments_Fact", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MedicalAppointments_Fact_Dim_Hospital_HospitalId",
                        column: x => x.HospitalId,
                        principalTable: "Dim_Hospital",
                        principalColumn: "HospitalId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MedicalAppointments_Fact_Dim_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Dim_Locations",
                        principalColumn: "LocationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MedicalAppointments_Fact_Dim_Medic_MedicId",
                        column: x => x.MedicId,
                        principalTable: "Dim_Medic",
                        principalColumn: "MedicId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MedicalAppointments_Fact_Dim_Specialty_SpecialtyId",
                        column: x => x.SpecialtyId,
                        principalTable: "Dim_Specialty",
                        principalColumn: "SpecialtyId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MedicalAppointments_Fact_Dim_Time_TimeId",
                        column: x => x.TimeId,
                        principalTable: "Dim_Time",
                        principalColumn: "TimeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MedicalAppointments_Fact_HospitalId",
                table: "MedicalAppointments_Fact",
                column: "HospitalId");

            migrationBuilder.CreateIndex(
                name: "IX_MedicalAppointments_Fact_LocationId",
                table: "MedicalAppointments_Fact",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_MedicalAppointments_Fact_MedicId",
                table: "MedicalAppointments_Fact",
                column: "MedicId");

            migrationBuilder.CreateIndex(
                name: "IX_MedicalAppointments_Fact_SpecialtyId",
                table: "MedicalAppointments_Fact",
                column: "SpecialtyId");

            migrationBuilder.CreateIndex(
                name: "IX_MedicalAppointments_Fact_TimeId",
                table: "MedicalAppointments_Fact",
                column: "TimeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MedicalAppointments_Fact");

            migrationBuilder.DropTable(
                name: "Dim_Hospital");

            migrationBuilder.DropTable(
                name: "Dim_Locations");

            migrationBuilder.DropTable(
                name: "Dim_Medic");

            migrationBuilder.DropTable(
                name: "Dim_Specialty");

            migrationBuilder.DropTable(
                name: "Dim_Time");
        }
    }
}
