﻿using MC.Domain.Base;
using MC.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MC.Domain.Models
{
    public class Appoitment : BaseEntity
    {
        
        public string Title { get; set; }
        public int UserId { get; set; }
        public Medic Medic { get; set; }
        public MedicCenter MedicCenter { get; set; }
        public AppointStatus Status { get; set; }
        public DateTime AppoitmenDate { get; set; }
    }
}
