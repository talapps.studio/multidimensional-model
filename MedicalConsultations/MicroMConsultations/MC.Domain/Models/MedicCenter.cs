﻿using MC.Domain.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MC.Domain.Models
{
    public class MedicCenter : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public MedicCenterType Type { get; set; }
        public int LocationId { get; set; }
        [ForeignKey("LocationId")]
        public Location Location { get; set; }
    }
}
