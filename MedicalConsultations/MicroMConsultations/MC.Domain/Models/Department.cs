﻿using MC.Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MC.Domain.Models
{
       
    public class Department : BaseEntity
    {
        public string Name { get; set; }
    }
}
