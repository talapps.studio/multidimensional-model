﻿using MC.Domain.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MC.Domain.Models
{
    public class Medic : BaseEntity
    {
        public string Name { get; set; }
        public string Lastname { get; set; }
        public string Phone { get; set; }

        public int SpecialtyId { get; set; }

        [ForeignKey("SpecialtyId")]
        public Specialty Specialty { get; set; }
    }
}
