﻿using MC.Domain.Base;

namespace MC.Domain.Models
{
    public class MedicCenterType : BaseEntity
    {
        public string Name { get; set; }
    }
}