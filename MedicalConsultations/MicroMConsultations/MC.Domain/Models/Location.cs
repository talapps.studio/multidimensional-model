﻿using MC.Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MC.Domain.Models
{
    public class Location : BaseEntity
    {
        public string Address { get; set; }

        public Department Department { get; set; }
        public City City { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}
