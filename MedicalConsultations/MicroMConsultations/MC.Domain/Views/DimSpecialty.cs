﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MC.Domain.Views
{
    public class DimSpecialty
    {
        [Key]
        public int SpecialtyId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string SubSpecialty { get; set; }
    }
}
