﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MC.Domain.Views
{
    public class MedicalAppointmentsFact
    {
        public int Id { get; set; }
       
        
        [ForeignKey("SpecialtyId")] public DimSpecialty Specialty { get; set; }

        [ForeignKey("MedicId")] public DimMedic Medic { get; set; }

        [ForeignKey("HospitalId")] public DimHospital Hospital { get; set; }

        [ForeignKey("LocationId")] public DimLocations Location { get; set; }
        [ForeignKey("TimeId")] public DimTime Time { get; set; }

        public int SpecialtyId { get; set; }
        public int LocationId { get; set; }
        public int HospitalId { get; set; }
        public int TimeId { get; set; }
        public int MedicId { get; set; }
        public int UserId { get; set; }
        public int ScheduledAppointments { get; set; }
        public int ConfirmedAppointments { get; set; }
        public int CancelledAppointments { get; set; }
    }
}
