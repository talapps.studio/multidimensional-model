﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MC.Domain.Views
{
    public class DimMedic
    {
        public int MedicId { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public int SpecialtyId { get; set; }
        public string SpecialtyName { get; set; }

    }
}
