﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MC.Domain.Views
{
    public class DimHospital
    {
        [Key]
        public int HospitalId { get; set; }
        public string Name { get; set; }
        public string Department { get; set; }
        public string Type { get; set; }
    }
}
