﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MC.Domain.Enum
{
    public enum AppointStatus
    {
        Aceppted,
        Programmed,
        Canceled,
        Reprogrammed
    }
}
